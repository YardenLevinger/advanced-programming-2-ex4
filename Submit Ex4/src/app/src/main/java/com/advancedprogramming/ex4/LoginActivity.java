package com.advancedprogramming.ex4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        final Button connectButton = (Button) findViewById(R.id.btnConnect);

        // Set the connect button listener
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), JoystickActivity.class);
                EditText ipTextBox = (EditText) findViewById(R.id.tbIP);
                String ip = ipTextBox.getText().toString();
                EditText portTextBox = (EditText) findViewById(R.id.tbPort);
                int port = Integer.parseInt(portTextBox.getText().toString());
                intent.putExtra("ip", ip);
                intent.putExtra("port", port);
                startActivity(intent);
            }
        });
    }
}
