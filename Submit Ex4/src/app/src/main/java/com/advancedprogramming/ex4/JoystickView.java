package com.advancedprogramming.ex4;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import androidx.annotation.RequiresApi;

public class JoystickView extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener {
    private Paint mPaint;
    private static final float maxVal = 1;
    private static final float minVal = -1;

    private static int R = 100; // The little circle radius
    private float centerX; //The circle center's X
    private float centerY; //The circle center's Y
    private float borderX; //An elipse Border
    private float borderY; //An elipse Border
    private double a2; //The elipse constants (a^2)
    private double b2; //The elipse constants (b^2)

    private int prevHeight;
    private int prevWidth;

    private JoystickListener listener;


    public JoystickView(Context context) {
        super(context);
        initialize(context);
    }

    public JoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public JoystickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        if (context instanceof JoystickListener)
            listener = (JoystickListener) context;
        getHolder().addCallback(this);
        this.setOnTouchListener(this);
    }

    private float getMinX() {
        return this.borderX + R;
    }

    private float getMaxX() {
        return this.getWidth() - getMinX();
    }

    private float getMinY() {
        return this.borderY + R;
    }

    private float getMaxY() {
        return this.getHeight() - getMinY();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        prevHeight = this.getHeight();
        prevWidth = this.getWidth();
        initializeMembers(this.getHeight(), this.getWidth());
    }


    // Initializing all the relevant members from the elipse and the little circle.
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void initializeMembers(int height, int width) {
        prevWidth = width;
        prevHeight = height;

        centerX = width / 2f;
        centerY = height / 2f;
        borderX = width / 10f;
        borderY = height / 10f;
        R=Math.min(height,width)/10;

        // We want the the whole circle will always be inside the elipse and not just the center
        // so we considered the circle radius in the elipse's constants.
        a2 = Math.pow((width / 2f) - borderX - R, 2);
        b2 = Math.pow((height / 2f) - borderY - R, 2);
        drawJoystick();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void drawJoystick() {
        getHolder().setSizeFromLayout();

        // Checks if the view's size has changed.
        if (this.getWidth() != prevWidth || this.getHeight() != prevHeight) {
            initializeMembers(this.getHeight(), this.getWidth());
        }

        if (getHolder().getSurface().isValid()) {
            Canvas canvas = this.getHolder().lockCanvas();
            mPaint = new Paint();

            // Clears the canvas
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

            // Fill the background color.
            canvas.drawColor(Color.parseColor("#FF03A9F4"));

            // Draw the elipse
            mPaint.setARGB(255, 200, 200, 200);
            canvas.drawOval(borderX, borderY, this.getWidth() - borderX, this.getHeight() - borderY, mPaint);

            // Draw the little circle
            mPaint.setARGB(255, 50, 200, 50);
            canvas.drawCircle(centerX, centerY, R, mPaint);

            // Release the canvas
            this.getHolder().unlockCanvasAndPost(canvas);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.equals(view)) {
            if (motionEvent.getAction() != motionEvent.ACTION_UP) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();

                double ellipse = Math.pow((x - (this.getWidth() / 2f)), 2) / a2 + Math.pow((y - (this.getHeight() / 2f)), 2) / b2;

                // Checks if the circle is outside the elipse.
                if (ellipse > 1) {
                    return true;
                }
                // Update the new circle center.
                centerX = x;
                centerY = y;

            }
            else
            {
                centerX = this.getWidth() / 2f;
                centerY = this.getHeight() / 2f;
//                listener.onJoystickMoved(0, 0);
            }

            // Calculate the Aileron and the Elevator.
            float newAileron = convertValuse(centerX, true);
            float newElevator = convertValuse(centerY, false);
            newElevator *= -1; // Because pixels in the bottom have bigger values than the pixels on the top.

            // Notify the listener about this (notufy that the joystick moved).
            listener.onJoystickMoved(newAileron, newElevator);

            // Update the canvas.
            drawJoystick();
        }
        return true;
    }


    public float convertValuse(float num, boolean isAileron) {
        float oldMin;
        float oldMax;
        if (isAileron) {
            oldMin = getMinX();
            oldMax = getMaxX();
        } else {
            oldMin = getMinY();
            oldMax = getMaxY();
        }
        float oldRange = oldMax - oldMin;
        float newRange = (maxVal - minVal);

        float newValue = (((num - oldMin) * newRange) / oldRange) + minVal;

        Log.d("Error", "" + newValue);
        return newValue;
    }

    public interface JoystickListener {
        void onJoystickMoved(float x, float y);
    }
}
