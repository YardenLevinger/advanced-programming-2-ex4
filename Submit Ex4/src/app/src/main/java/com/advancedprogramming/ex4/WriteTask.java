package com.advancedprogramming.ex4;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.OutputStream;

public class WriteTask extends AsyncTask<String, String, Object> {

    private OutputStream outputStream;

    public WriteTask(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override
    protected Object doInBackground(String... messages) {
        String message = messages[0];
        try {
            outputStream.write(message.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
