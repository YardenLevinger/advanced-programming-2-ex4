package com.advancedprogramming.ex4;

import java.io.IOException;

public class JoystickHandler implements JoystickView.JoystickListener {
    private String ip;
    private int port;
    private SimulatorClient client;

    public JoystickHandler(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public void startClient() {
        this.client = new SimulatorClient();
        this.client.startClient(this.ip, this.port);
    }

    public void endClient(){
        this.client.close();
    }

    @Override
    public void onJoystickMoved(float xPercent, float yPercent) {

        String aileronMessage = "set " + "/controls/flight/aileron " + xPercent + "\r\n";
        String elevatorMessage = "set " + "/controls/flight/elevator " + yPercent + "\r\n";
        try {
            this.client.write(aileronMessage);
            this.client.write(elevatorMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
