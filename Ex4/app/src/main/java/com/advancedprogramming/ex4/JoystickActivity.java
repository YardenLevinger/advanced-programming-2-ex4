package com.advancedprogramming.ex4;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;

public class JoystickActivity extends AppCompatActivity implements JoystickView.JoystickListener {

    private JoystickHandler joystickHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
        Intent instant = getIntent();

        //Get the extras from the intant
        String ip = instant.getStringExtra("ip");
        int port = instant.getIntExtra("port", 0);

        //Create The Handler
        this.joystickHandler = new JoystickHandler(ip, port);
        this.joystickHandler.startClient();
        setContentView(R.layout.joystick_activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.joystickHandler.endClient();
    }

    @Override
    public void onJoystickMoved(float xPercent, float yPercent) {
        // Call the handler
        this.joystickHandler.onJoystickMoved(xPercent, yPercent);
    }
}
