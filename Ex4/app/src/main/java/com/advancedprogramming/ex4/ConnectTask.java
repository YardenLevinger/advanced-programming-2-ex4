package com.advancedprogramming.ex4;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ConnectTask extends AsyncTask<String, JoystickHandler, Socket> {
    @Override
    protected Socket doInBackground(String... strings) {
        Socket socket = null;
        String ip = strings[0];
        int port = Integer.parseInt(strings[1]);
        InetAddress serverAdd = null;
        try {
            serverAdd = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            socket = new Socket(serverAdd, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return socket;
    }
}
