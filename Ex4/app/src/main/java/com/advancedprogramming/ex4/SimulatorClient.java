package com.advancedprogramming.ex4;

import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class SimulatorClient {

    private OutputStream output;
    private Socket clientSocket;

    public void startClient(String ip, int port) {
        try {
            ConnectTask connectTask = new ConnectTask();
            connectTask.execute(ip, port + "");
            this.clientSocket = connectTask.get();
            try {
                this.output = this.clientSocket.getOutputStream();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.e("TCP", "C:Error", e);
        }
    }

    public void write(String message) throws IOException {
        WriteTask writeTask = new WriteTask(this.output);
        writeTask.execute(message);
    }

    public void close()
    {
        try {
            this.output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
